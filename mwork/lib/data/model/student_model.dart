class student {
  String? studentId;
  String? studentName;
  String? email;
  String? avatar;
  String? status;

  student(
      {this.studentId, this.studentName, this.email, this.avatar, this.status});

  student.fromJson(Map<String, dynamic> json) {
    studentId = json['student_id'];
    studentName = json['student_name'];
    email = json['email'];
    avatar = json['avatar'];
    status = json['status'];
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['student_id'] = this.studentId;
    data['student_name'] = this.studentName;
    data['email'] = this.email;
    data['avatar'] = this.avatar;
    data['status'] = this.status;
    return data;
  }
}
