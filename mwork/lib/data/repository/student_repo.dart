import 'package:get/get.dart';
import 'package:mwork/data/api/api_client.dart';

import '../../utils/app_constraints.dart';

class StudentRepo extends GetxService{
  final ApiClient apiClient;
  StudentRepo({required this.apiClient});

  Future<Response> getStudentList() async {
    return await apiClient.getData(AppConstrants.STUDENT_URI);
  }
}