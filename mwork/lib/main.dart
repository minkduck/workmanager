import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:get/get_navigation/src/root/get_material_app.dart';
import 'package:mwork/page/account/account_page.dart';
import 'package:mwork/page/auth/google_sign_in.dart';
import 'package:mwork/page/auth/login_home_page.dart';
import 'package:mwork/page/auth/login_page.dart';
import 'package:mwork/test/login_widget.dart';
import 'package:mwork/page/home/home_page.dart';
import 'package:mwork/test/notification_push.dart';
import 'package:mwork/test/upload_file.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_messaging/firebase_messaging.dart';

Future<void> backgroundHandler(RemoteMessage message) async {
  print("This is message from background");
  print(message.notification!.title);
  print(message.notification!.body);
}

Future main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();

  FirebaseMessaging.onBackgroundMessage(backgroundHandler);
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) => ChangeNotifierProvider(
      create: (context) => GoogleSignInProvider(),
      child: GetMaterialApp(
      debugShowCheckedModeBanner: false,
      title: 'mWork',
      theme: ThemeData(

        primarySwatch: Colors.blue,
      ),
      home: PushNotification(),
    )
  );
}
