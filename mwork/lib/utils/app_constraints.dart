class AppConstrants{
  static const String APP_NAME = "DBFood";
  static const int APP_VERSION = 1;

  static const String BASE_URL = "https://capstone-matching.herokuapp.com";
  static const String POPULAR_PRODUCT_URI = "/api/v1/products/popular";
  static const String RECOMMEND_PRODUCT_URI = "/api/v1/products/recommended";
  static const String UPLOAD_URL = "/uploads/";
  static const String STUDENT_URI = "/api/v1/students";


  //auth end points
  static const String REGISTRATION_URI = "/api/v1/auth/register";

  static const String TOKEN = "DBtoken";
  static const String CART_LIST ="Cart-list";
  static const String CART_HISTORY_LIST ="Cart-history-list";


}