import 'package:get/get_connect/http/src/response/response.dart';
import 'package:get/get_state_manager/get_state_manager.dart';
import 'package:mwork/data/model/student_model.dart';
import 'package:mwork/data/repository/student_repo.dart';

class StudentController extends GetxController {
  final StudentRepo studentRepo;
  StudentController({required this.studentRepo});
  List<dynamic> _studentList = [];
  List<dynamic> get studentList => _studentList;

  Future<void> getStudentList() async {
    Response response = await studentRepo.getStudentList();
    if(response.statusCode == 200){
      _studentList = [];
      _studentList.add(student.fromJson(response.body));
      update();
    }else{
      print(response.statusCode);
    }
  }
}