import 'package:get/get.dart';
import 'package:get/get_core/src/get_main.dart';
import 'package:mwork/data/controllers/student_controller.dart';
import 'package:mwork/data/repository/student_repo.dart';

import '../data/api/api_client.dart';
import '../utils/app_constraints.dart';

Future<void> init() async {

  //api client
  Get.lazyPut(() => ApiClient(appBaseUrl: AppConstrants.BASE_URL));

  //repos
  Get.lazyPut(() => StudentRepo(apiClient: Get.find()));

  //controllers
  Get.lazyPut(() => StudentController(studentRepo: Get.find()));

}