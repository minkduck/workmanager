import 'dart:io';

import 'package:file_picker/file_picker.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:gap/gap.dart';
import 'package:mwork/utils/app_layout.dart';

class UploadFile extends StatefulWidget {
  const UploadFile({Key? key}) : super(key: key);

  @override
  State<UploadFile> createState() => _UploadFileState();
}

class _UploadFileState extends State<UploadFile> {
  PlatformFile? pickedFIle;
  UploadTask? uploadTask;

  Future selectFile() async {
    final result = await FilePicker.platform.pickFiles(allowMultiple: false);
    if (result == null) return;

    setState(() {
      pickedFIle = result.files.first;
    });
  }

  Future uploadFile() async {
    final path = 'files/${pickedFIle!.name}';
    final file = File(pickedFIle!.path!);

    final ref = FirebaseStorage.instance.ref().child(path);
    setState ((){
      uploadTask = ref.putFile(file);
    });

    final snapshot = await uploadTask!.whenComplete(() {});

    final urlDownload = await snapshot.ref.getDownloadURL();
    print('Download Link: $urlDownload');
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Upload file"),
        centerTitle: true,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            if (pickedFIle != null)
              Expanded(
                child: Container(
                  color: Colors.blue[100],
                    child: Image.file(
                      File(pickedFIle!.path!),
                    width: double.infinity,
                    fit: BoxFit.cover,
                    ),
                ),
              ),
            Gap(AppLayout.getHeight(32)),
            ElevatedButton(
                onPressed: selectFile,
                child: const Text('Select File')),
            Gap(AppLayout.getHeight(32)),
            ElevatedButton(
                onPressed: uploadFile,
                child: const Text('Upload File')),
            buildProgress(),
          ],
        ),
      ),
    );
  }

  Widget buildProgress() => StreamBuilder<TaskSnapshot>(
      stream: uploadTask?.snapshotEvents,
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          final data = snapshot.data!;
          double progress = data.bytesTransferred / data.totalBytes;

          return SizedBox(
            height: 50,
            child: Stack(
              fit: StackFit.expand,
              children: [
                LinearProgressIndicator(
                  value: progress,
                  backgroundColor: Colors.grey,
                  color: Colors.green,
                ),
                Center(
                  child: Text(
                    '${(100 * progress).roundToDouble()}%',
                    style: const TextStyle(color: Colors.white),
                  ),
                )
              ],
            ),
          );

        } else {
          return const SizedBox(height: 50,);
        }
      },
  );
}
