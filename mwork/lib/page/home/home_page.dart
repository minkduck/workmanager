import 'package:flutter/material.dart';
import 'package:mwork/page/account/account_page.dart';
import 'package:mwork/page/home/contract_page.dart';
import 'package:mwork/page/home/home_screen.dart';
import 'package:mwork/page/message/message_page.dart';
import 'package:mwork/page/home/proposal_page.dart';

import '../../utils/colors.dart';

class HomePage extends StatefulWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  int _selectIndex = 0;
  List pages =[
    HomeScreen(),
    ProposalPage(),
    ContractPage(),
    Messagepage(),
    AccountPage()
  ];

  void onTapNav(int index){
    setState(() {
      _selectIndex = index;
    });
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: pages[_selectIndex],
      bottomNavigationBar: BottomNavigationBar(
        selectedItemColor: AppColors.mainColor,
        unselectedItemColor: Colors.amberAccent,
        showSelectedLabels: false,
        showUnselectedLabels: false,
        currentIndex: _selectIndex,
        onTap: onTapNav,
        items: const [
          BottomNavigationBarItem(
              icon: Icon(Icons.home_outlined,),
              label: "Home"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.insert_page_break_outlined,),
              label: "Proposal"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.request_page_outlined,),
              label: "Contract"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.message_outlined,),
              label: "Message"
          ),
          BottomNavigationBarItem(
              icon: Icon(Icons.person,),
              label: "Me"
          ),
        ],
      ),
    );
  }
}